// Code generated by protoc-gen-go-grpc. DO NOT EDIT.
// versions:
// - protoc-gen-go-grpc v1.2.0
// - protoc             v3.21.12
// source: courier_service.proto

package courier_service

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
	emptypb "google.golang.org/protobuf/types/known/emptypb"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

// CourierServiceClient is the client API for CourierService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type CourierServiceClient interface {
	Create(ctx context.Context, in *CreateCourier, opts ...grpc.CallOption) (*Courier, error)
	GetById(ctx context.Context, in *CourierPrimaryKey, opts ...grpc.CallOption) (*Courier, error)
	GetList(ctx context.Context, in *GetListCourierRequest, opts ...grpc.CallOption) (*GetListCourierResponse, error)
	Update(ctx context.Context, in *UpdateCourier, opts ...grpc.CallOption) (*Courier, error)
	UpdatePatch(ctx context.Context, in *UpdatePatchCourier, opts ...grpc.CallOption) (*Courier, error)
	Delete(ctx context.Context, in *CourierPrimaryKey, opts ...grpc.CallOption) (*emptypb.Empty, error)
}

type courierServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewCourierServiceClient(cc grpc.ClientConnInterface) CourierServiceClient {
	return &courierServiceClient{cc}
}

func (c *courierServiceClient) Create(ctx context.Context, in *CreateCourier, opts ...grpc.CallOption) (*Courier, error) {
	out := new(Courier)
	err := c.cc.Invoke(ctx, "/courier_service.CourierService/Create", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *courierServiceClient) GetById(ctx context.Context, in *CourierPrimaryKey, opts ...grpc.CallOption) (*Courier, error) {
	out := new(Courier)
	err := c.cc.Invoke(ctx, "/courier_service.CourierService/GetById", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *courierServiceClient) GetList(ctx context.Context, in *GetListCourierRequest, opts ...grpc.CallOption) (*GetListCourierResponse, error) {
	out := new(GetListCourierResponse)
	err := c.cc.Invoke(ctx, "/courier_service.CourierService/GetList", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *courierServiceClient) Update(ctx context.Context, in *UpdateCourier, opts ...grpc.CallOption) (*Courier, error) {
	out := new(Courier)
	err := c.cc.Invoke(ctx, "/courier_service.CourierService/Update", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *courierServiceClient) UpdatePatch(ctx context.Context, in *UpdatePatchCourier, opts ...grpc.CallOption) (*Courier, error) {
	out := new(Courier)
	err := c.cc.Invoke(ctx, "/courier_service.CourierService/UpdatePatch", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *courierServiceClient) Delete(ctx context.Context, in *CourierPrimaryKey, opts ...grpc.CallOption) (*emptypb.Empty, error) {
	out := new(emptypb.Empty)
	err := c.cc.Invoke(ctx, "/courier_service.CourierService/Delete", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// CourierServiceServer is the server API for CourierService service.
// All implementations must embed UnimplementedCourierServiceServer
// for forward compatibility
type CourierServiceServer interface {
	Create(context.Context, *CreateCourier) (*Courier, error)
	GetById(context.Context, *CourierPrimaryKey) (*Courier, error)
	GetList(context.Context, *GetListCourierRequest) (*GetListCourierResponse, error)
	Update(context.Context, *UpdateCourier) (*Courier, error)
	UpdatePatch(context.Context, *UpdatePatchCourier) (*Courier, error)
	Delete(context.Context, *CourierPrimaryKey) (*emptypb.Empty, error)
	mustEmbedUnimplementedCourierServiceServer()
}

// UnimplementedCourierServiceServer must be embedded to have forward compatible implementations.
type UnimplementedCourierServiceServer struct {
}

func (UnimplementedCourierServiceServer) Create(context.Context, *CreateCourier) (*Courier, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Create not implemented")
}
func (UnimplementedCourierServiceServer) GetById(context.Context, *CourierPrimaryKey) (*Courier, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetById not implemented")
}
func (UnimplementedCourierServiceServer) GetList(context.Context, *GetListCourierRequest) (*GetListCourierResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetList not implemented")
}
func (UnimplementedCourierServiceServer) Update(context.Context, *UpdateCourier) (*Courier, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Update not implemented")
}
func (UnimplementedCourierServiceServer) UpdatePatch(context.Context, *UpdatePatchCourier) (*Courier, error) {
	return nil, status.Errorf(codes.Unimplemented, "method UpdatePatch not implemented")
}
func (UnimplementedCourierServiceServer) Delete(context.Context, *CourierPrimaryKey) (*emptypb.Empty, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Delete not implemented")
}
func (UnimplementedCourierServiceServer) mustEmbedUnimplementedCourierServiceServer() {}

// UnsafeCourierServiceServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to CourierServiceServer will
// result in compilation errors.
type UnsafeCourierServiceServer interface {
	mustEmbedUnimplementedCourierServiceServer()
}

func RegisterCourierServiceServer(s grpc.ServiceRegistrar, srv CourierServiceServer) {
	s.RegisterService(&CourierService_ServiceDesc, srv)
}

func _CourierService_Create_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CreateCourier)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(CourierServiceServer).Create(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/courier_service.CourierService/Create",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(CourierServiceServer).Create(ctx, req.(*CreateCourier))
	}
	return interceptor(ctx, in, info, handler)
}

func _CourierService_GetById_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CourierPrimaryKey)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(CourierServiceServer).GetById(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/courier_service.CourierService/GetById",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(CourierServiceServer).GetById(ctx, req.(*CourierPrimaryKey))
	}
	return interceptor(ctx, in, info, handler)
}

func _CourierService_GetList_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetListCourierRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(CourierServiceServer).GetList(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/courier_service.CourierService/GetList",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(CourierServiceServer).GetList(ctx, req.(*GetListCourierRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _CourierService_Update_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(UpdateCourier)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(CourierServiceServer).Update(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/courier_service.CourierService/Update",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(CourierServiceServer).Update(ctx, req.(*UpdateCourier))
	}
	return interceptor(ctx, in, info, handler)
}

func _CourierService_UpdatePatch_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(UpdatePatchCourier)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(CourierServiceServer).UpdatePatch(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/courier_service.CourierService/UpdatePatch",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(CourierServiceServer).UpdatePatch(ctx, req.(*UpdatePatchCourier))
	}
	return interceptor(ctx, in, info, handler)
}

func _CourierService_Delete_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CourierPrimaryKey)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(CourierServiceServer).Delete(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/courier_service.CourierService/Delete",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(CourierServiceServer).Delete(ctx, req.(*CourierPrimaryKey))
	}
	return interceptor(ctx, in, info, handler)
}

// CourierService_ServiceDesc is the grpc.ServiceDesc for CourierService service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var CourierService_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "courier_service.CourierService",
	HandlerType: (*CourierServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "Create",
			Handler:    _CourierService_Create_Handler,
		},
		{
			MethodName: "GetById",
			Handler:    _CourierService_GetById_Handler,
		},
		{
			MethodName: "GetList",
			Handler:    _CourierService_GetList_Handler,
		},
		{
			MethodName: "Update",
			Handler:    _CourierService_Update_Handler,
		},
		{
			MethodName: "UpdatePatch",
			Handler:    _CourierService_UpdatePatch_Handler,
		},
		{
			MethodName: "Delete",
			Handler:    _CourierService_Delete_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "courier_service.proto",
}
