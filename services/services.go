package services

import (
	"gitlab.com/order7695732/ecommers_go_api_gateway/config"
	"gitlab.com/order7695732/ecommers_go_api_gateway/genproto/category_service"
	"gitlab.com/order7695732/ecommers_go_api_gateway/genproto/courier_service"
	"gitlab.com/order7695732/ecommers_go_api_gateway/genproto/customer_service"
	"gitlab.com/order7695732/ecommers_go_api_gateway/genproto/order_service"
	"gitlab.com/order7695732/ecommers_go_api_gateway/genproto/product_service"
	"gitlab.com/order7695732/ecommers_go_api_gateway/genproto/user_service"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type ServiceManagerI interface {
	UserService() user_service.UserServiceClient
	ProductService() product_service.ProductServiceClient
	CustomerService() customer_service.CustomerServiceClient
	CourierService() courier_service.CourierServiceClient
	CategoryService() category_service.CategoryServiceClient
	OrderService() order_service.OrderServiceClient
}

type grpcClients struct {
	userService     user_service.UserServiceClient
	productService  product_service.ProductServiceClient
	customerService customer_service.CustomerServiceClient
	courierService  courier_service.CourierServiceClient
	categoryService category_service.CategoryServiceClient
	orderService   order_service.OrderServiceClient
}

func NewGrpcClient(cfg config.Config) (ServiceManagerI, error) {
	// Order Microservice
	connOrderService, err := grpc.Dial(
		cfg.OrderServiceHost+cfg.OrderGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)

	if err != nil {
		return nil, err
	}

	return &grpcClients{
		userService:     user_service.NewUserServiceClient(connOrderService),
		productService:  product_service.NewProductServiceClient(connOrderService),
		customerService: customer_service.NewCustomerServiceClient(connOrderService),
		categoryService: category_service.NewCategoryServiceClient(connOrderService),
		courierService: courier_service.NewCourierServiceClient(connOrderService),
		orderService: order_service.NewOrderServiceClient(connOrderService),
	}, nil
}

func (g *grpcClients) UserService() user_service.UserServiceClient {
	return g.userService
}

func (g *grpcClients) ProductService() product_service.ProductServiceClient {
	return g.productService
}
func (g *grpcClients) CustomerService() customer_service.CustomerServiceClient {
	return g.customerService
}
func (g *grpcClients) CourierService() courier_service.CourierServiceClient {
	return g.courierService
}
func (g *grpcClients) CategoryService() category_service.CategoryServiceClient {
	return g.categoryService
}

func (g *grpcClients) OrderService() order_service.OrderServiceClient {
	return g.orderService
}
