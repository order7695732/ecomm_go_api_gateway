package handler

import (
	"context"

	"github.com/gin-gonic/gin"
	"gitlab.com/order7695732/ecommers_go_api_gateway/api/http"
	"gitlab.com/order7695732/ecommers_go_api_gateway/genproto/courier_service"
	"gitlab.com/order7695732/ecommers_go_api_gateway/pkg/util"
)

// CreateCourier godoc
// @ID create_courier
// @Router /courier [POST]
// @Summary Create Courier
// @Description  Create Courier
// @Tags Courier
// @Accept json
// @Produce json
// @Param profile body courier_service.CreateCourier true "CreateCourierRequestBody"
// @Success 200 {object} http.Response{data=courier_service.Courier} "GetCourierBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateCourier(c *gin.Context) {

	var courier courier_service.CreateCourier

	err := c.ShouldBindJSON(&courier)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.CourierService().Create(
		c.Request.Context(),
		&courier,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.Created, resp)
}

// GetCourierByID godoc
// @ID get_courier_by_id
// @Router /courier/{id} [GET]
// @Summary Get Courier  By ID
// @Description Get Courier  By ID
// @Tags Courier
// @Accept json
// @Produce json
// @Param id path string false "id"
// @Success 200 {object} http.Response{data=courier_service.Courier} "CourierBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetCourierByID(c *gin.Context) {

	courierID := c.Param("id")

	if !util.IsValidUUID(courierID) {
		h.handleResponse(c, http.InvalidArgument, "courier id is an invalid uuid")
		return
	}

	resp, err := h.services.CourierService().GetById(
		context.Background(),
		&courier_service.CourierPrimaryKey{
			Id: courierID,
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// GetCourierList godoc
// @ID get_courier_list
// @Router /courier [GET]
// @Summary Get Courier s List
// @Description  Get Courier s List
// @Tags Courier
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param search query string false "search"
// @Success 200 {object} http.Response{data=courier_service.GetListCourierResponse} "GetAllCourierResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetCourierList(c *gin.Context) {

	offset, err := h.getOffsetParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	resp, err := h.services.CourierService().GetList(
		context.Background(),
		&courier_service.GetListCourierRequest{
			Limit:  int64(limit),
			Offset: int64(offset),
			Search: c.Query("search"),
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// UpdateCourier godoc
// @ID update_courier
// @Router /courier/{id} [PUT]
// @Summary Update Courier
// @Description Update Courier
// @Tags Courier
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body courier_service.UpdateCourier true "UpdateCourierRequestBody"
// @Success 200 {object} http.Response{data=courier_service.Courier} "Courier data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateCourier(c *gin.Context) {

	var courier courier_service.UpdateCourier

	courier.Id = c.Param("id")

	if !util.IsValidUUID(courier.Id) {
		h.handleResponse(c, http.InvalidArgument, "courier id is an invalid uuid")
		return
	}

	err := c.ShouldBindJSON(&courier)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.CourierService().Update(
		c.Request.Context(),
		&courier,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// DeleteCourier godoc
// @ID delete_courier
// @Router /courier/{id} [DELETE]
// @Summary Delete Courier
// @Description Delete Courier
// @Tags Courier
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "Courier data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteCourier(c *gin.Context) {

	courierId := c.Param("id")

	if !util.IsValidUUID(courierId) {
		h.handleResponse(c, http.InvalidArgument, "courier id is an invalid uuid")
		return
	}

	resp, err := h.services.CourierService().Delete(
		c.Request.Context(),
		&courier_service.CourierPrimaryKey{Id: courierId},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.NoContent, resp)
}
