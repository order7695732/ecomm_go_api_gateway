package handler

import (
	"context"

	"github.com/gin-gonic/gin"
	"gitlab.com/order7695732/ecommers_go_api_gateway/api/http"
	"gitlab.com/order7695732/ecommers_go_api_gateway/genproto/category_service"
	"gitlab.com/order7695732/ecommers_go_api_gateway/pkg/util"
)

// CreateCategory godoc
// @ID create_category
// @Router /category [POST]
// @Summary Create Category
// @Description  Create Category
// @Tags Category
// @Accept json
// @Produce json
// @Param profile body category_service.CreateCategory true "CreateCategoryRequestBody"
// @Success 200 {object} http.Response{data=category_service.Category} "GetCategoryBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateCategory(c *gin.Context) {

	var category category_service.CreateCategory

	err := c.ShouldBindJSON(&category)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.CategoryService().Create(
		c.Request.Context(),
		&category,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.Created, resp)
}

// GetCategoryByID godoc
// @ID get_category_by_id
// @Router /category/{id} [GET]
// @Summary Get Category  By ID
// @Description Get Category  By ID
// @Tags Category
// @Accept json
// @Produce json
// @Param id path string false "id"
// @Success 200 {object} http.Response{data=category_service.Category} "CategoryBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetCategoryByID(c *gin.Context) {

	categoryID := c.Param("id")

	if !util.IsValidUUID(categoryID) {
		h.handleResponse(c, http.InvalidArgument, "category id is an invalid uuid")
		return
	}

	resp, err := h.services.CategoryService().GetById(
		context.Background(),
		&category_service.CategoryPrimaryKey{
			Id: categoryID,
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// GetCategoryList godoc
// @ID get_category_list
// @Router /category [GET]
// @Summary Get Category s List
// @Description  Get Category s List
// @Tags Category
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param search query string false "search"
// @Success 200 {object} http.Response{data=category_service.GetListCategoryResponse} "GetAllCategoryResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetCategoryList(c *gin.Context) {

	offset, err := h.getOffsetParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	resp, err := h.services.CategoryService().GetList(
		context.Background(),
		&category_service.GetListCategoryRequest{
			Limit:  int64(limit),
			Offset: int64(offset),
			Search: c.Query("search"),
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// UpdateCategory godoc
// @ID update_category
// @Router /category/{id} [PUT]
// @Summary Update Category
// @Description Update Category
// @Tags Category
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body category_service.UpdateCategory true "UpdateCategoryRequestBody"
// @Success 200 {object} http.Response{data=category_service.Category} "Category data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateCategory(c *gin.Context) {

	var category category_service.UpdateCategory

	category.Id = c.Param("id")

	if !util.IsValidUUID(category.Id) {
		h.handleResponse(c, http.InvalidArgument, "category id is an invalid uuid")
		return
	}

	err := c.ShouldBindJSON(&category)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.CategoryService().Update(
		c.Request.Context(),
		&category,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// DeleteCategory godoc
// @ID delete_category
// @Router /category/{id} [DELETE]
// @Summary Delete Category
// @Description Delete Category
// @Tags Category
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "Category data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteCategory(c *gin.Context) {

	categoryId := c.Param("id")

	if !util.IsValidUUID(categoryId) {
		h.handleResponse(c, http.InvalidArgument, "category id is an invalid uuid")
		return
	}

	resp, err := h.services.CategoryService().Delete(
		c.Request.Context(),
		&category_service.CategoryPrimaryKey{Id: categoryId},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.NoContent, resp)
}
