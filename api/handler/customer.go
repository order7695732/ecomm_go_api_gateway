package handler

import (
	"context"

	"github.com/gin-gonic/gin"
	"gitlab.com/order7695732/ecommers_go_api_gateway/api/http"
	"gitlab.com/order7695732/ecommers_go_api_gateway/genproto/customer_service"
	"gitlab.com/order7695732/ecommers_go_api_gateway/pkg/util"
)

// CreateCustomer godoc
// @ID create_customer
// @Router /customer [POST]
// @Summary Create Customer
// @Description  Create Customer
// @Tags Customer
// @Accept json
// @Produce json
// @Param profile body customer_service.CreateCustomer true "CreateCustomerRequestBody"
// @Success 200 {object} http.Response{data=customer_service.Customer} "GetCustomerBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateCustomer(c *gin.Context) {

	var customer customer_service.CreateCustomer

	err := c.ShouldBindJSON(&customer)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.CustomerService().Create(
		c.Request.Context(),
		&customer,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.Created, resp)
}

// GetCustomerByID godoc
// @ID get_customer_by_id
// @Router /customer/{id} [GET]
// @Summary Get Customer  By ID
// @Description Get Customer  By ID
// @Tags Customer
// @Accept json
// @Produce json
// @Param id path string false "id"
// @Success 200 {object} http.Response{data=customer_service.Customer} "CustomerBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetCustomerByID(c *gin.Context) {

	customerID := c.Param("id")

	if !util.IsValidUUID(customerID) {
		h.handleResponse(c, http.InvalidArgument, "customer id is an invalid uuid")
		return
	}

	resp, err := h.services.CustomerService().GetById(
		context.Background(),
		&customer_service.CustomerPrimaryKey{
			Id: customerID,
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// GetCustomerList godoc
// @ID get_customer_list
// @Router /customer [GET]
// @Summary Get Customer s List
// @Description  Get Customer s List
// @Tags Customer
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param search query string false "search"
// @Success 200 {object} http.Response{data=customer_service.GetListCustomerResponse} "GetAllCustomerResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetCustomerList(c *gin.Context) {

	offset, err := h.getOffsetParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	resp, err := h.services.CustomerService().GetList(
		context.Background(),
		&customer_service.GetListCustomerRequest{
			Limit:  int64(limit),
			Offset: int64(offset),
			Search: c.Query("search"),
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// UpdateCustomer godoc
// @ID update_customer
// @Router /customer/{id} [PUT]
// @Summary Update Customer
// @Description Update Customer
// @Tags Customer
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body customer_service.UpdateCustomer true "UpdateCustomerRequestBody"
// @Success 200 {object} http.Response{data=customer_service.Customer} "Customer data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateCustomer(c *gin.Context) {

	var customer customer_service.UpdateCustomer

	customer.Id = c.Param("id")

	if !util.IsValidUUID(customer.Id) {
		h.handleResponse(c, http.InvalidArgument, "customer id is an invalid uuid")
		return
	}

	err := c.ShouldBindJSON(&customer)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.CustomerService().Update(
		c.Request.Context(),
		&customer,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// DeleteCustomer godoc
// @ID delete_customer
// @Router /customer/{id} [DELETE]
// @Summary Delete Customer
// @Description Delete Customer
// @Tags Customer
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "Customer data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteCustomer(c *gin.Context) {

	customerId := c.Param("id")

	if !util.IsValidUUID(customerId) {
		h.handleResponse(c, http.InvalidArgument, "customer id is an invalid uuid")
		return
	}

	resp, err := h.services.CustomerService().Delete(
		c.Request.Context(),
		&customer_service.CustomerPrimaryKey{Id: customerId},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.NoContent, resp)
}
