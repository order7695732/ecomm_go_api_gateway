package main

import (
	"fmt"

	"github.com/gin-gonic/gin"
	"gitlab.com/order7695732/ecommers_go_api_gateway/api"
	"gitlab.com/order7695732/ecommers_go_api_gateway/api/handler"
	"gitlab.com/order7695732/ecommers_go_api_gateway/config"
	"gitlab.com/order7695732/ecommers_go_api_gateway/pkg/logger"
	"gitlab.com/order7695732/ecommers_go_api_gateway/services"
)

func main() {
	cfg := config.Load()

	fmt.Println("config: %+v", cfg)

	grpcSrvc, err := services.NewGrpcClient(cfg)

	if err != nil {
		panic(err)
	}

	loggerLevel := logger.LevelDebug

	switch cfg.Environment {
	case config.DebugMode:
		loggerLevel = logger.LevelDebug
	case config.TestMode:
		loggerLevel = logger.LevelDebug
	default:
		loggerLevel = logger.LevelInfo
	}

	log := logger.NewLogger(cfg.ServiceName, loggerLevel)
	defer logger.Cleanup(log)

	r := gin.New()

	r.Use(gin.Logger(), gin.Recovery())

	h := handler.NewHandler(cfg, log, grpcSrvc)

	api.SetUpAPI(r, h, cfg)

	fmt.Println("Start Api-Gateway ")

	err = r.Run(cfg.HTTPPort)

	if err != nil {
		return
	}

}
